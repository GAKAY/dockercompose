package com.personal.service;

import org.springframework.stereotype.Service;

import com.personal.data.TestData;

@Service
public interface GetTestDataService {

	public TestData getTestData(String firstName);

	public String postTestData(TestData data);
}

/*
 * Copyright 2018 Capital One Financial Corporation All Rights Reserved.
 * 
 * This software contains valuable trade secrets and proprietary information of
 * Capital One and is protected by law. It may not be copied or distributed in
 * any form or medium, disclosed to third parties, reverse engineered or used in
 * any manner without prior written authorization from Capital One.
 */
