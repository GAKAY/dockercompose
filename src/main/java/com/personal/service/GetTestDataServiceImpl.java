package com.personal.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.personal.data.TestData;
import com.personal.data.TestDataRepo;

public class GetTestDataServiceImpl implements GetTestDataService {

	@Autowired
	private TestDataRepo testDataRepo;

	@Override
	public TestData getTestData(String id) {
		Optional<TestData> data = testDataRepo.findById(id);
		return data.get();

	}

	@Override
	public String postTestData(TestData data) {
		TestData t1 = testDataRepo.save(data);
		if (t1 != null) {
			return t1.getId();
		}
		return "ERROR.. CAN'T SAVE";

	}

}

/*
 * Copyright 2018 Capital One Financial Corporation All Rights Reserved.
 * 
 * This software contains valuable trade secrets and proprietary information of
 * Capital One and is protected by law. It may not be copied or distributed in
 * any form or medium, disclosed to third parties, reverse engineered or used in
 * any manner without prior written authorization from Capital One.
 */
