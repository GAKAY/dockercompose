package com.personal.resource;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.personal.data.TestData;
import com.personal.service.GetTestDataService;

@RestController
@RequestMapping("/sample")
public class MainResource {

	@Autowired
	GetTestDataService service;

	@GetMapping(path = "/{name}", produces = "application/json")
	public TestData getMethod(@PathVariable("name") String firstName) {
		System.out.println("###  NAME RECEIVED ###" + firstName);
		return service.getTestData(firstName);
	}

	@PostMapping(path = "/post", consumes = { "application/json" }, produces = { "application/json" })
	public String postMethod(@RequestBody TestData data) {
		data.setId(UUID.randomUUID().toString());
		System.out.println("###  Data RECEIVED ###" + data.toString());
		return service.postTestData(data);
	}

}