package com.personal.config;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import com.personal.service.GetTestDataService;
import com.personal.service.GetTestDataServiceImpl;

@SpringBootConfiguration
public class AppConfig {

	RedisStandaloneConfiguration standaloneoconfig() {
		RedisStandaloneConfiguration standaloneoconfig = new RedisStandaloneConfiguration("redis", 6379);
		return standaloneoconfig;
	}

	@Bean
	JedisConnectionFactory jedisConnFactory() {

		JedisConnectionFactory jcf = new JedisConnectionFactory(standaloneoconfig());
		return jcf;

	}

	@Bean
	RedisTemplate<String, Object> redisTemplate() {
		RedisTemplate<String, Object> redisTemplate = new RedisTemplate<String, Object>();
		redisTemplate.setConnectionFactory(jedisConnFactory());

		return redisTemplate;

	}

	@Bean
	GetTestDataService getTestDataService() {
		return new GetTestDataServiceImpl();

	}

}

/*
 * Copyright 2018 Capital One Financial Corporation All Rights Reserved.
 * 
 * This software contains valuable trade secrets and proprietary information of
 * Capital One and is protected by law. It may not be copied or distributed in
 * any form or medium, disclosed to third parties, reverse engineered or used in
 * any manner without prior written authorization from Capital One.
 */
