FROM java:8
COPY /build/libs/Springboot-1.0.jar /opt
WORKDIR /opt
EXPOSE 8080
CMD ["java", "-jar","Springboot-1.0.jar"]
